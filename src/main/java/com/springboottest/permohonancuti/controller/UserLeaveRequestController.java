package com.springboottest.permohonancuti.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.io.iona.springboot.controllers.HibernateCRUDController;
import com.springboottest.permohonancuti.dto.UserLeaveRequestDTO;
import com.springboottest.permohonancuti.model.UserLeaveRequest;

@RestController
@RequestMapping("/api/userleaverequest")
public class UserLeaveRequestController extends HibernateCRUDController<UserLeaveRequest, UserLeaveRequestDTO> {

}
