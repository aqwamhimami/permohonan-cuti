package com.springboottest.permohonancuti.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.io.iona.springboot.controllers.HibernateViewController;
import com.springboottest.permohonancuti.dto.ViewListRequestLeaveDTO;
import com.springboottest.permohonancuti.model.ViewListRequestLeave;

@RestController
@RequestMapping("/api/listrequestleave")
public class ViewListRequestLeaveController extends HibernateViewController<ViewListRequestLeave, ViewListRequestLeaveDTO> {

}
