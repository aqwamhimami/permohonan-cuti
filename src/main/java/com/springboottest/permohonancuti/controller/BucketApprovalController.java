package com.springboottest.permohonancuti.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.io.iona.springboot.controllers.HibernateCRUDController;
import com.springboottest.permohonancuti.dto.BucketApprovalDTO;
import com.springboottest.permohonancuti.model.BucketApproval;

@RestController
@RequestMapping("/api/bucketapproval")
public class BucketApprovalController extends HibernateCRUDController<BucketApproval, BucketApprovalDTO> {

}
