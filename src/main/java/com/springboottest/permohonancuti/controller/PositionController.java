package com.springboottest.permohonancuti.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.io.iona.springboot.controllers.HibernateCRUDController;
import com.springboottest.permohonancuti.dto.PositionDTO;
import com.springboottest.permohonancuti.model.Position;

@RestController
@RequestMapping("/api/position")
public class PositionController extends HibernateCRUDController<Position, PositionDTO>{

}
