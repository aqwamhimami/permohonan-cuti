package com.springboottest.permohonancuti.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.io.iona.springboot.controllers.HibernateCRUDController;
import com.springboottest.permohonancuti.dto.PositionLeaveDTO;
import com.springboottest.permohonancuti.model.PositionLeave;

@RestController
@RequestMapping("/api/positionleave")
public class PositionLeaveController extends HibernateCRUDController<PositionLeave, PositionLeaveDTO> {

}
