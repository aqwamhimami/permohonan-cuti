package com.springboottest.permohonancuti.controller.process;

import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboottest.permohonancuti.dto.BucketApprovalDTO;
import com.springboottest.permohonancuti.dto.UserDTO;
import com.springboottest.permohonancuti.dto.UserLeaveRequestDTO;
import com.springboottest.permohonancuti.dto.process.RequestLeaveDTO;
import com.springboottest.permohonancuti.model.BucketApproval;
import com.springboottest.permohonancuti.model.User;
import com.springboottest.permohonancuti.model.UserLeaveRequest;
import com.springboottest.permohonancuti.repositories.BucketApprovalRepository;
import com.springboottest.permohonancuti.repositories.UserLeaveRequestRepository;
import com.springboottest.permohonancuti.repositories.UserRepository;

@RestController
@RequestMapping("/api")
public class  RequestLeaveController {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	UserLeaveRequestRepository userLeaveRequestRepository;
	
	@Autowired
    BucketApprovalRepository bucketApprovalRepository;
    ModelMapper modelMapper = new ModelMapper();

    // ADD NEW
    @PostMapping("/requestleave")
    public Map<String, Object> createLeaveRequest(@Valid @RequestBody RequestLeaveDTO requestLeaveDTO){
        Map<String, Object> result = new HashMap<String,Object>();
        long userId = requestLeaveDTO.getUserId();
        int sisaJatahcuti = setSisaCuti(userId);
        long today = new Date().getTime();
        long diff = requestLeaveDTO.getLeaveDateTo().getTime() - requestLeaveDTO.getLeaveDateFrom().getTime();
        long dayRequestLeave = diff / (24 * 60 * 60 * 1000);
        
        String stringDateFrom = requestLeaveDTO.getLeaveDateFrom().getDate() +"-"+ (requestLeaveDTO.getLeaveDateFrom().getMonth()+1) +"-"+(requestLeaveDTO.getLeaveDateFrom().getYear()+1900);
        String stringDateTo = requestLeaveDTO.getLeaveDateTo().getDate() +"-"+ (requestLeaveDTO.getLeaveDateTo().getMonth()+1) +"-"+(requestLeaveDTO.getLeaveDateTo().getYear()+1900);
        
        if (sisaJatahcuti <= 0) {
        	result.put("message", "Mohon maaf, jatah cuti anda telah habis.");
		}
        else if(dayRequestLeave <= 0) {
        	result.put("message", "Tanggal yang Anda ajukan tidak valid.");
        }
        else if (dayRequestLeave > sisaJatahcuti) {
        	result.put("message", "Mohon maaf, jatah cuti Anda tidak cukup untuk digunakan dari tanggal "+stringDateFrom+ " sampai "+ stringDateTo +" (" + dayRequestLeave + " hari). Jatah cuti Anda yang tersisa adalah " + sisaJatahcuti + " hari");
		}
        else if (requestLeaveDTO.getLeaveDateFrom().getTime() < today) {
        	result.put("message", "Tanggal yang Anda ajukan telah lampau, silahkan ganti tanggal pengajuan cuti anda.");
		}
        else {

            User user = userRepository.getOne(userId);
            UserDTO userDTO = modelMapper.map(user, UserDTO.class);
            
            BucketApprovalDTO bucketApprovalDTO = new BucketApprovalDTO();
            BucketApproval bucketApprovalEntity = new BucketApproval();
            
            bucketApprovalDTO.setCreatedBy(user.getUserName());
            bucketApprovalDTO.setCreatedDate(new Date());
            bucketApprovalDTO.setDescription(requestLeaveDTO.getDescription());
            bucketApprovalDTO.setStatus("waiting");
            bucketApprovalDTO.setUser(userDTO);

            bucketApprovalEntity = modelMapper.map(bucketApprovalDTO, BucketApproval.class);
            bucketApprovalRepository.save(bucketApprovalEntity);

            UserLeaveRequestDTO userLeaveRequestDTO = new UserLeaveRequestDTO();
            UserLeaveRequest userLeaveRequestEntity = new UserLeaveRequest();
            
            userLeaveRequestDTO.setBucketApprovalId(bucketApprovalEntity.getBucketApprovalId());
            userLeaveRequestDTO.setCreatedBy(user.getUserName());
            userLeaveRequestDTO.setCreatedDate(new Date());
            userLeaveRequestDTO.setLeaveDateFrom(requestLeaveDTO.getLeaveDateFrom());
            userLeaveRequestDTO.setLeaveDateTo(requestLeaveDTO.getLeaveDateTo());
            userLeaveRequestDTO.setSisaCuti(sisaJatahcuti);
            userLeaveRequestDTO.setStatus("wating");
            userLeaveRequestDTO.setUser(userDTO);
            
            userLeaveRequestEntity = modelMapper.map(userLeaveRequestDTO, UserLeaveRequest.class);
            userLeaveRequestRepository.save(userLeaveRequestEntity);
            
            result.put("message", "Permohonan Anda sedang diproses.");
        }

        return result;
    }
    
    private int setSisaCuti(long userId) {
    	List<UserLeaveRequest> userLeaveRequestList = userLeaveRequestRepository.findAll();
    	boolean isEmpty = true;
    	int sisaCuti=0, sisaCutiTemp;
    	for (UserLeaveRequest userLeaveRequest : userLeaveRequestList) {
			if (userLeaveRequest.getUser().getUserId() == userId) {
				sisaCutiTemp = userLeaveRequest.getSisaCuti();
				if (sisaCuti==0) {
					sisaCuti = sisaCutiTemp;
				}
				if (sisaCuti > sisaCutiTemp) {
					sisaCuti = sisaCutiTemp;
				}
				isEmpty = false;
			}
		}
    	if (isEmpty) {
			sisaCuti = userRepository.getOne(userId).getPosition().getPositionLeave().getJatahCuti();
		}
    	return sisaCuti;
    }
    
}
