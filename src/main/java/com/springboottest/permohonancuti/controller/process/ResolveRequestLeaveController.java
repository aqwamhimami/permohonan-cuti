package com.springboottest.permohonancuti.controller.process;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboottest.permohonancuti.dto.BucketApprovalDTO;
import com.springboottest.permohonancuti.dto.UserLeaveRequestDTO;
import com.springboottest.permohonancuti.dto.process.ResolveRequestLeaveDTO;
import com.springboottest.permohonancuti.model.BucketApproval;
import com.springboottest.permohonancuti.model.UserLeaveRequest;
import com.springboottest.permohonancuti.repositories.BucketApprovalRepository;
import com.springboottest.permohonancuti.repositories.UserLeaveRequestRepository;

@RestController
@RequestMapping("/api")
public class ResolveRequestLeaveController {
	
	@Autowired
    UserLeaveRequestRepository userLeaveRequestRepository;
    ModelMapper modelMapper = new ModelMapper();
    
    @Autowired
    BucketApprovalRepository bucketApprovalRepository;
  
    // UPDATE
    @PostMapping("/resolverequestleave")
    public Map<String, Object> updateBucketApproval(@Valid @RequestBody ResolveRequestLeaveDTO resolveRequestLeaveDTO) {
        Map<String, Object> result = new HashMap<String, Object>();
        
        String accepted = "accepted";
        String status = resolveRequestLeaveDTO.getStatus();
        
        boolean isBucketApprovalPresent = bucketApprovalRepository.findById(resolveRequestLeaveDTO.getBucketApprovalId()).isPresent();
        
        if (isBucketApprovalPresent) {
            BucketApproval bucketApprovalEntity = bucketApprovalRepository.findById(resolveRequestLeaveDTO.getBucketApprovalId()).get();
            BucketApprovalDTO bucketApprovalDTO = new BucketApprovalDTO();
            
        	bucketApprovalDTO = modelMapper.map(bucketApprovalEntity, BucketApprovalDTO.class);
    		
            bucketApprovalDTO.setNote(resolveRequestLeaveDTO.getResolverReason());
            bucketApprovalDTO.setStatus(status);
            bucketApprovalDTO.setUpdatedBy(resolveRequestLeaveDTO.getResolvedBy());
            bucketApprovalDTO.setUpdatedDate(resolveRequestLeaveDTO.getResolvedDate());
            
            bucketApprovalEntity = modelMapper.map(bucketApprovalDTO, BucketApproval.class);
            bucketApprovalEntity.setBucketApprovalId(bucketApprovalDTO.getBucketApprovalId());
            bucketApprovalRepository.save(bucketApprovalEntity);
            
            UserLeaveRequest userLeaveRequestEntity = new UserLeaveRequest();
            UserLeaveRequestDTO userLeaveRequestDTO = new UserLeaveRequestDTO();
            List<UserLeaveRequest> userLeaveRequestList = userLeaveRequestRepository.findAll();
            
            for (UserLeaveRequest userLeaveRequest : userLeaveRequestList) {
    			if (resolveRequestLeaveDTO.getBucketApprovalId() == userLeaveRequest.getBucketApprovalId()) {
    				userLeaveRequestDTO = modelMapper.map(userLeaveRequest, UserLeaveRequestDTO.class);
    			}
    		}
            
            long createDateTimestamp = userLeaveRequestDTO.getCreatedDate().getTime();
            long resolveDateTimestamp = resolveRequestLeaveDTO.getResolvedDate().getTime();
            long leaveDateFromTimestamp = userLeaveRequestDTO.getLeaveDateFrom().getTime();
            
            if (resolveDateTimestamp > leaveDateFromTimestamp) {
            	result.put("message", "Kesalahan data, tanggal keputusan tidak bisa setelah tanggal yang diajukan untuk cuti.");
			} 
            else if (resolveDateTimestamp < createDateTimestamp) {
            	result.put("message", "Kesalahan data, tanggal keputusan tidak bisa lebih awal dari pengajuan cuti.");
			}
            else {
	            int sisaCuti = userLeaveRequestDTO.getSisaCuti();
	            
	            userLeaveRequestDTO.setStatus(status);
	            userLeaveRequestDTO.setUpdatedBy(resolveRequestLeaveDTO.getResolvedBy());
	            userLeaveRequestDTO.setUpdatedDate(resolveRequestLeaveDTO.getResolvedDate());
	            
	            if (status.equalsIgnoreCase(accepted)) {
	            	sisaCuti = calculationSisaCuti(userLeaveRequestDTO.getLeaveDateFrom(), userLeaveRequestDTO.getLeaveDateTo(), sisaCuti);
	    			userLeaveRequestDTO.setSisaCuti(sisaCuti);
	    		}
	            
	            userLeaveRequestEntity = modelMapper.map(userLeaveRequestDTO, UserLeaveRequest.class);
	            userLeaveRequestEntity.setUserLeaveRequestId(userLeaveRequestDTO.getUserLeaveRequestId());
	            userLeaveRequestRepository.save(userLeaveRequestEntity);

	            result.put("message", "Permohonan dengan ID "+ resolveRequestLeaveDTO.getBucketApprovalId() +" telah berhasil diputuskan");
				
			}
            
		} 
        else if (isBucketApprovalPresent == false) {
        	result.put("message", "Permohonan dengan ID "+ resolveRequestLeaveDTO.getBucketApprovalId() +" tidak ditemukan");
		}
        else {
        	result.put("message", "Permohonan gagal");
        }
        return result;
    }
    
    private int calculationSisaCuti(Date dateFrom, Date dateTo, int sisaCuti) {
    	long diffDate = dateFrom.getTime() - dateTo.getTime();
    	long dayRequestLeave = diffDate / (24 * 60 * 60 * 1000);
    	sisaCuti -= dayRequestLeave;
    	return sisaCuti;
    }
    
}
