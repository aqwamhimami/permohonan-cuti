package com.springboottest.permohonancuti.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.io.iona.springboot.controllers.HibernateCRUDController;
import com.springboottest.permohonancuti.dto.UserDTO;
import com.springboottest.permohonancuti.model.User;

@RestController
@RequestMapping("/api/user")
public class UserController extends HibernateCRUDController<User, UserDTO>{

}
