package com.springboottest.permohonancuti.dto;

import java.util.Date;

public class PositionLeaveDTO {
	
	private long positionLeaveId;
	private int jatahCuti;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	public long getPositionLeaveId() {
		return positionLeaveId;
	}
	public void setPositionLeaveId(long positionLeaveId) {
		this.positionLeaveId = positionLeaveId;
	}
	public int getJatahCuti() {
		return jatahCuti;
	}
	public void setJatahCuti(int jatahCuti) {
		this.jatahCuti = jatahCuti;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
		
}
