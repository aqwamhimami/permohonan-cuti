package com.springboottest.permohonancuti.dto;

import java.util.Date;

import com.springboottest.permohonancuti.model.User;

public class UserLeaveRequestDTO {
	private long userLeaveRequestId;
	private UserDTO user;
	private String status;
	private Date leaveDateFrom;
	private Date leaveDateTo;
	private int sisaCuti;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	private long bucketApprovalId;
	
	public long getUserLeaveRequestId() {
		return userLeaveRequestId;
	}
	public void setUserLeaveRequestId(long userLeaveRequestId) {
		this.userLeaveRequestId = userLeaveRequestId;
	}
	public UserDTO getUser() {
		return user;
	}
	public void setUser(UserDTO user) {
		this.user = user;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getLeaveDateFrom() {
		return leaveDateFrom;
	}
	public void setLeaveDateFrom(Date leaveDateFrom) {
		this.leaveDateFrom = leaveDateFrom;
	}
	public Date getLeaveDateTo() {
		return leaveDateTo;
	}
	public void setLeaveDateTo(Date leaveDateTo) {
		this.leaveDateTo = leaveDateTo;
	}
	public int getSisaCuti() {
		return sisaCuti;
	}
	public void setSisaCuti(int sisaCuti) {
		this.sisaCuti = sisaCuti;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public long getBucketApprovalId() {
		return bucketApprovalId;
	}
	public void setBucketApprovalId(long bucketApprovalId) {
		this.bucketApprovalId = bucketApprovalId;
	}
	
}
