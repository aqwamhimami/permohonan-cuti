package com.springboottest.permohonancuti.dto.process;

import java.util.Date;

public class ResolveRequestLeaveDTO {
	private long bucketApprovalId;
	private String status;
	private String resolverReason;
	private String resolvedBy;
	private Date resolvedDate;
	
	public long getBucketApprovalId() {
		return bucketApprovalId;
	}
	public void setBucketApprovalId(long bucketApprovalId) {
		this.bucketApprovalId = bucketApprovalId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getResolverReason() {
		return resolverReason;
	}
	public void setResolverReason(String resolverReason) {
		this.resolverReason = resolverReason;
	}
	public String getResolvedBy() {
		return resolvedBy;
	}
	public void setResolvedBy(String resolvedBy) {
		this.resolvedBy = resolvedBy;
	}
	public Date getResolvedDate() {
		return resolvedDate;
	}
	public void setResolvedDate(Date resolvedDate) {
		this.resolvedDate = resolvedDate;
	}
}
