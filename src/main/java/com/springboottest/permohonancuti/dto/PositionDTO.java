package com.springboottest.permohonancuti.dto;

import java.util.Date;

import com.springboottest.permohonancuti.model.PositionLeave;

public class PositionDTO {
	private long positionId;
	private PositionLeave positionLeave;
	private String positionName;
	private int positionLevel;
	private String positionCode;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	public long getPositionId() {
		return positionId;
	}
	public void setPositionId(long positionId) {
		this.positionId = positionId;
	}
	public PositionLeave getPositionLeave() {
		return positionLeave;
	}
	public void setPositionLeave(PositionLeave positionLeave) {
		this.positionLeave = positionLeave;
	}
	public String getPositionName() {
		return positionName;
	}
	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}
	public int getPositionLevel() {
		return positionLevel;
	}
	public void setPositionLevel(int positionLevel) {
		this.positionLevel = positionLevel;
	}
	public String getPositionCode() {
		return positionCode;
	}
	public void setPositionCode(String positionCode) {
		this.positionCode = positionCode;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
