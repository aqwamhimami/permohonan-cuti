package com.springboottest.permohonancuti.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springboottest.permohonancuti.model.PositionLeave;

@Repository
public interface PositionLeaveRepository extends JpaRepository<PositionLeave, Long>{

}
