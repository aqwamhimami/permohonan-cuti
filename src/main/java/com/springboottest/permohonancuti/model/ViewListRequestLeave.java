package com.springboottest.permohonancuti.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name = "view_list_request_leave", schema = "public")
public class ViewListRequestLeave {
	
	private int no;
	private long userId;
	private Date leaveDateFrom;
	private Date leaveDateTo;
	private String description;
	private String note;
	private String updatedBy;
	private Date updatedDate;
	
	@Id
	@Column(name = "no", unique = true, nullable = false)
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	
	@Column(name = "user_id", nullable = false)
	public long getUserId() {
		return this.userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "leave_date_from", length = 13)
	public Date getLeaveDateFrom() {
		return this.leaveDateFrom;
	}

	public void setLeaveDateFrom(Date leaveDateFrom) {
		this.leaveDateFrom = leaveDateFrom;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "leave_date_to", length = 13)
	public Date getLeaveDateTo() {
		return this.leaveDateTo;
	}

	public void setLeaveDateTo(Date leaveDateTo) {
		this.leaveDateTo = leaveDateTo;
	}
	
	@Column(name = "resolver_reason")
	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@Column(name = "description")
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "resolved_by", length = 50)
	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "resolved_date", length = 13)
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
