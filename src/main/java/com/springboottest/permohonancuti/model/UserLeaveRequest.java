package com.springboottest.permohonancuti.model;
// Generated Jan 21, 2020 7:22:20 AM by Hibernate Tools 5.1.10.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * UserLeaveRequest generated by hbm2java
 */
@Entity
@Table(name = "user_leave_request", schema = "public")
public class UserLeaveRequest implements java.io.Serializable {

	private long userLeaveRequestId;
	private User user;
	private String status;
	private Date leaveDateFrom;
	private Date leaveDateTo;
	private int sisaCuti;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	private long bucketApprovalId;

	public UserLeaveRequest() {
	}

	public UserLeaveRequest(long userLeaveRequestId, User user) {
		this.userLeaveRequestId = userLeaveRequestId;
		this.user = user;
	}

	public UserLeaveRequest(long userLeaveRequestId, User user, String status, Date leaveDateFrom, Date leaveDateTo,
			int sisaCuti, String createdBy, Date createdDate, String updatedBy, Date updatedDate,long bucketApprovalId) {
		this.userLeaveRequestId = userLeaveRequestId;
		this.user = user;
		this.status = status;
		this.leaveDateFrom = leaveDateFrom;
		this.leaveDateTo = leaveDateTo;
		this.sisaCuti = sisaCuti;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
		this.bucketApprovalId = bucketApprovalId;
	}

	@Id
	@SequenceGenerator(name="user_leave_request_gen", sequenceName="user_leave_request_seq", schema = "public", allocationSize = 1)
 	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_leave_request_gen")
	@Column(name = "user_leave_request_id", unique = true, nullable = false)
	public long getUserLeaveRequestId() {
		return this.userLeaveRequestId;
	}

	public void setUserLeaveRequestId(long userLeaveRequestId) {
		this.userLeaveRequestId = userLeaveRequestId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable = false)
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Column(name = "status", length = 10)
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "leave_date_from", length = 13)
	public Date getLeaveDateFrom() {
		return this.leaveDateFrom;
	}

	public void setLeaveDateFrom(Date leaveDateFrom) {
		this.leaveDateFrom = leaveDateFrom;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "leave_date_to", length = 13)
	public Date getLeaveDateTo() {
		return this.leaveDateTo;
	}

	public void setLeaveDateTo(Date leaveDateTo) {
		this.leaveDateTo = leaveDateTo;
	}

	@Column(name = "sisa_cuti")
	public int getSisaCuti() {
		return this.sisaCuti;
	}

	public void setSisaCuti(int sisaCuti) {
		this.sisaCuti = sisaCuti;
	}

	@Column(name = "created_by", length = 50)
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "created_date", length = 13)
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "updated_by", length = 50)
	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "updated_date", length = 13)
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Column(name = "bucket_approval_id")
	public long getBucketApprovalId() {
		return bucketApprovalId;
	}

	public void setBucketApprovalId(long bucketApprovalId) {
		this.bucketApprovalId = bucketApprovalId;
	}
}
