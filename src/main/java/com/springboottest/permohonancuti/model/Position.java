package com.springboottest.permohonancuti.model;
// Generated Jan 21, 2020 7:22:20 AM by Hibernate Tools 5.1.10.Final

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Position generated by hbm2java
 */
@Entity
@Table(name = "position", schema = "public")
public class Position implements java.io.Serializable {

	private long positionId;
	private PositionLeave positionLeave;
	private String positionName;
	private int positionLevel;
	private String positionCode;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	private Set<User> users = new HashSet<User>(0);

	public Position() {
	}

	public Position(long positionId, PositionLeave positionLeave) {
		this.positionId = positionId;
		this.positionLeave = positionLeave;
	}

	public Position(long positionId, PositionLeave positionLeave, String positionName, int positionLevel,
			String positionCode, String createdBy, Date createdDate, String updatedBy, Date updatedDate,
			Set<User> users) {
		this.positionId = positionId;
		this.positionLeave = positionLeave;
		this.positionName = positionName;
		this.positionLevel = positionLevel;
		this.positionCode = positionCode;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
		this.users = users;
	}

	@Id
	@SequenceGenerator(name="position_gen", sequenceName="position_seq", schema = "public", allocationSize = 1)
 	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "position_gen")
	@Column(name = "position_id", unique = true, nullable = false)
	public long getPositionId() {
		return this.positionId;
	}

	public void setPositionId(long positionId) {
		this.positionId = positionId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "position_leave_id", nullable = false)
	public PositionLeave getPositionLeave() {
		return this.positionLeave;
	}

	public void setPositionLeave(PositionLeave positionLeave) {
		this.positionLeave = positionLeave;
	}

	@Column(name = "position_name", length = 50)
	public String getPositionName() {
		return this.positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	@Column(name = "position_level")
	public int getPositionLevel() {
		return this.positionLevel;
	}

	public void setPositionLevel(int positionLevel) {
		this.positionLevel = positionLevel;
	}

	@Column(name = "position_code", length = 50)
	public String getPositionCode() {
		return this.positionCode;
	}

	public void setPositionCode(String positionCode) {
		this.positionCode = positionCode;
	}

	@Column(name = "created_by", length = 50)
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "created_date", length = 13)
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "updated_by", length = 50)
	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "updated_date", length = 13)
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "position")
	public Set<User> getUsers() {
		return this.users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

}
